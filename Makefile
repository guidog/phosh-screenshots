check_requirements:
	@ ! (pip3 install --dry-run --break-system-packages -r requirements.txt | grep 'Would install')
test:
	$(MAKE) -C tools
